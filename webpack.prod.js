const merge = require('webpack-merge');
const common = require('./webpack.config.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

module.exports = merge(common, {
  mode: "production",

  plugins: [
    new webpack.EnvironmentPlugin({
      DEBUG: false,
      foo: "bar"
    }),

    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 2,
    }),

    new UglifyJsPlugin({
      uglifyOptions: {
        compress: true,
        mangle: true,
        output: {
          comments: false
        }
      }
    }),
  ]
});