import { Component } from 'squaredjs'

export default class AutomaticZOrdering extends Component {

  public update(delta: number): void {
    if (this.entity.parentLayer) {
      this.entity.sprite.setZOrder(this.entity.sprite.y + this.entity.sprite.height)
    }
  }

  public destroy(): void {
    //
  }

}
