import { Entity, EntitySettings, Spritesheet } from 'squaredjs'
import AutomaticZOrdering from '../components/automaticZOrdering'
import { COLLISION_TYPE } from '../../node_modules/squaredjs/dist/components/rigidBody'

export default class Zombie extends Entity {

  constructor(settings: EntitySettings) {
    super(settings)

    this.offset.x = 3
    this.offset.y = 8

    this.width = 10
    this.height = 8

    const tileset = new Spritesheet('assets/tilesets/0x72_16x16DungeonTileset.v4.png', 16, 16)
    this.addAnimation(tileset, 'idle', [[80, 160]], 1, { loop: true })

    // RigidBody
    this.body.friction.x = Number.MAX_SAFE_INTEGER
    this.body.friction.y = Number.MAX_SAFE_INTEGER
    this.body.collisionType = COLLISION_TYPE.ACTIVE

    new AutomaticZOrdering(this)
  }
}
