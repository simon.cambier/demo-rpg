import { Entity, App, EntitySettings, COLLISION_GROUP, COLLISION_TYPE } from 'squaredjs'
import Player from './player'
import * as TWEEN from '@tweenjs/tween.js'

export default class MapLoader extends Entity {

  constructor(settings: EntitySettings) {
    super(settings)
    this.body.collisionType = COLLISION_TYPE.PASSTHROUGH
  }

  public async onCollisionStart(other: Entity): Promise<void> {
    console.log('start')
    if (other instanceof Player) {

      // Disable user input
      other.allowInput = false

      // Animate the Player entity
      const tween = new TWEEN.Tween({ x: other.x, y: other.y })
        .onUpdate(o => {
          other.x = o.x
          other.y = o.y
        })
        .onComplete(async () => {
          // Load the new map
          const level = require('../levels/' + this.name + '.json')
          await App.instance.loadLevel(level)
        })
      let to: { x?: string, y?: string }
      const vel = other.body.vel

      switch (this.properties['direction']) {
        case 'up':
          to = { y: '-' + Math.abs(vel.y) }
          break
        case 'down':
          to = { y: '+' + Math.abs(vel.y) }
          break
        case 'left':
          to = { x: '-' + Math.abs(vel.x) }
          break
        case 'right':
          to = { x: '+' + Math.abs(vel.x) }
          break
        default:
          throw Error('invalid direction for MapLoader')
      }
      // console.log(to)
      tween.to(to, 1000).start()
    }
  }
}
