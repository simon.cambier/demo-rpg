import { Entity, EntitySettings, Spritesheet, Tools, App, CollisionTraceResult, COLLISION_TYPE } from 'squaredjs'
import { Vector2 } from '../../node_modules/squaredjs/dist/types'

export default class Beholder extends Entity {

  private destination = { x: 0, y: 0 }
  private speed = 25

  constructor(settings: EntitySettings) {
    super(settings)

    const tileset = new Spritesheet('assets/tilesets/0x72_16x16DungeonTileset.v4.png', 32, 32)
    this.addAnimation(tileset, 'idle', [[160, 176]], 1, { loop: true })
    this.body.collisionType = COLLISION_TYPE.FIXED

    this.offset = { x: 8, y: 8 }
  }

  public update(delta: number): void {
    super.update(delta)
    this.goto(this.destination)
    this.flip.x = this.body.vel.x > 0
  }

  public onMovement(res: CollisionTraceResult): void {
    if (res.collision.x || res.collision.y) {
      this.setRandomDestination()
    }
  }

  private setRandomDestination(): void {
    const level = App.instance.level
    this.destination = { x: Tools.randomRange(0, level.width), y: Tools.randomRange(0, level.height) }
  }

  private goto(dest: Vector2): void {
    const distance = Math.sqrt(Math.pow(dest.x - this.x, 2) + Math.pow(dest.y - this.y, 2))
    const directionX = (dest.x - this.x) / distance
    const directionY = (dest.y - this.y) / distance

    this.body.vel.x = directionX * this.speed
    this.body.vel.y = directionY * this.speed

    if (distance < 1) {
      this.setRandomDestination()
    }
  }
}
