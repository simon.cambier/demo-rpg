module.exports = {
  paths: {
    engine: 'src/engine',
    game: 'src/game',
    levels: 'src/game/levels',
    entities: 'src/game/entities',
    assets: 'assets',
    dist: 'dist'
  },
  editor: {
    minZoomLevel: 1,
    maxZoomLevel: 16
  }
}
